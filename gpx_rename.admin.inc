<?php
/**
 * @file
 * Configuration forms for the gpx_rename module.
 */

/**
 * Form that sets the directory containing the gpx files to rename.
 */
function gpx_rename_set_directory_form($form, &$form_state) {
  $form = array();
  $path = variable_get('gpx_rename_path');

  $form['gpx_rename_path'] = array(
    '#type' => 'textfield',
    '#size' => 80,
    '#title' => t('Enter the directory that contains the gpx files to rename.'),
    '#description' => t('The path can be relative, for example public://gpx or it can be an absolute system path, for example f:\xampp\htdocs\drupal\sites\default\files\gpx'),
    '#default_value' => $path,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Process directory'),
  );
  return $form;
}

/**
 * Submit function for the gpx_rename_set_directory_form.
 */
function gpx_rename_set_directory_form_submit($form, &$form_state) {
  $path = variable_get('gpx_rename_path');
  $uris = array();
  if (isset($form_state['values']['gpx_rename_path'])) {
    if ($path != $form_state['values']['gpx_rename_path']) {
      $path = $form_state['values']['gpx_rename_path'];
      variable_set('gpx_rename_path', $path);
    }
    $files = file_scan_directory($path, '/.*\.gpx$/i');
    foreach ($files as $file) {
    $uris[] = $file->uri;
    }
    gpx_rename_rewrite_files($uris);
  }
}
